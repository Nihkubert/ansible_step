Установка + настройка хоста:

Установим ubuntu-22.04.1 тут я думаю проблем не возникнет.
Скачивать вот отсюда (https://releases.ubuntu.com/22.04.1/ubuntu-22.04.1-desktop-amd64.iso?_ga=2.255464392.1933172364.1663830901-2048101691.1663830901)

Перед началом подготовки машины обновляем индекс пакетов
$ sudo apt-get update    

Устанавливаем пакеты питона 
$ apt-get install python2-minimal  

SSH порт
$ sudo iptables -A INPUT -p tcp --dport 22 -j ACCEPT

Узнаем IP хоста
$ sudo apt install net-tools 
$ sudo ifconfig
В моем случае IP 192.168.0.79
![image](https://user-images.githubusercontent.com/109202643/192238826-bc809cec-df2e-4027-b74f-e333086b7dca.png)


Установка + настройка Ansible сервера:

Установим ubuntu-22.04.1 тут я думаю проблем не возникнет.
Скачивать вот отсюда (https://releases.ubuntu.com/22.04.1/ubuntu-22.04.1-desktop-amd64.iso?_ga=2.255464392.1933172364.1663830901-2048101691.1663830901)

Перед началом подготовки машины обновляем индекс пакетов
$ sudo apt-get update    

Установка SSH-сервер
$ sudo apt install openssh-server
$ sudo systemctl enable sshd

Создадим SSH ключ  скопируем его на хост
$ ssh-keygen
![image](https://user-images.githubusercontent.com/109202643/192238954-7181bd7e-c25b-4c01-b124-7c78b63ad8fc.png)


скопируем его на хост
$ cat ~/.ssh/id_rsa.pub | ssh <user>@<hostname> 'cat >> .ssh/authorized_keys && echo "Key copied"'
  ![image](https://user-images.githubusercontent.com/109202643/192239008-461e1a4e-d862-454c-86a7-066068355a1b.png)


Установка Ansible
$ sudo apt-get -y install ansible

Загружаем репозиторий файлы с гита (https://github.com/Nihkubert/ansible_step.git)
  понадобится SSH ключ пишите на почту alextrebuhin54@gmail.com я вам его вышлю))))

Настраиваем файлы ansible.cfg 
host_key_checking = False
inventory = /etc/ansible/hosts

Настраиваем файлы  hosts
Укажите IP-адрес и имя необходимого сервера. 
[webservers]
lamp ansible_host=192.168.0.79
# Укажите учетные данные для веб-сервера. 
[webservers:vars]
ansible_user=aadmin
ansible_ssh_pass=45010605
ansible_ssh_private_key_file=.ssh/id_rsa

Снятие защиты и установка Wordpress + LAMP
ansible-vault расшифровывает /etc/ansible/playbooks/playbook.yml
ansible-playbook /etc/ansible/playbooks/playbook.yml --ask-vault-pass

  ![image](https://user-images.githubusercontent.com/109202643/192238385-48a60f0f-5c32-45d5-abe3-f8fd8ef8b740.png)

